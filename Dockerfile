FROM ubuntu
WORKDIR /app
RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev
COPY . ./
RUN chmod a+x *.py
ENTRYPOINT ["./hello.py"]
